import { useParams } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';
import { useEffect, useState } from 'react';

export default function SpecificCourse(){

    // const {_id, name, description, price} = courseProp
//useParams() contains any values we are trying to pass in the URL, stored as a key/value pair
//e.g.
//courseId: 620e3a092868e6460da110d7
    const[name,setName] = useState("")
    const[description,setDescription] = useState("")
    const[price,setPrice] = useState("")

    const { courseId } = useParams();

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
           // console.log(data)
           setName(data.name)
           setDescription(data.description)
           setPrice(data.price)
        })
	}, [])

    return(
        <Card className= "my-3">
            <Card.Header className="bg-dark text-white text-center pb-0">
                <h4>{name}</h4>
            </Card.Header>
            <Card.Body>
                <Card.Text>{description}</Card.Text>
                <h6>Price: {price}</h6>
            </Card.Body>
            <Card.Footer className="d-grid gap-2">
                <Button variant="primary" block="true">Enroll</Button>
            </Card.Footer>
        </Card>
    )
}